<?php

$butterfly = 'Hello World!';

$css_arr = call_css();
$time_hour_arr = call_time_arr();


function h($str, $encoding='UTF-8') {
	return htmlspecialchars($str, ENT_QUOTES, $encoding);
}

function call_css() {
	$css_arr[] = '<link rel="stylesheet" href="assets/css/reset.css">';
	$css_arr[] = '<link rel="stylesheet" href="assets/css/index.css">';
	return $css_arr;
}

function call_time_arr() {
	$time_arr = [
		'18', '19', '20', '21', '22', '23', '24', '25',
		'26', '27', '28', '29', '30' ];
	return $time_arr;
}