<?php include('butterfly.php'); ?>

<html>
    <?php
        foreach($css_arr as $call_css) {
            echo $call_css;
        }
    ?>

<body>

    <form class="bf_form" method="">
        <div class="item-form">
            <button class="clear">clear</button>
        </div>
        <div class="item-form">
			<label>日付</label>
			<input type="date"></input>
		</div>
		
		<div class="item-form">
			<label>同伴</label>
            <select type="dohan">
                <option value="0">なし</option>
                <option value="1">あり</option>
            </select>
		</div>
		
        <div class="item-form">
            <label>出勤</label>
            <select type="start_time_hour">
				<?php foreach($time_hour_arr as $hour) { ?>
					<option value="<?=$hour?>"><?=$hour?></option>
				<?php } ?>
			</select>
			:
            <select type="start_time_min">
                <option value="0">0</option>
                <option value="15">15</option>
                <option value="30">30</option>
                <option value="45">45</option>
            </select>
		</div>
		
        <div class="item-form">
            <label>退勤</label>
            <select type="end_time_hour">
				<?php foreach($time_hour_arr as $hour) { ?>
					<option value="<?=$hour?>"><?=$hour?></option>
				<?php } ?>
			</select>
			:
            <select type="end_time_min">
                <option value="0">0</option>
                <option value="15">15</option>
                <option value="30">30</option>
                <option value="45">45</option>
            </select>
		</div>

		<div class="item-form">
			<label>時給</label>
			<input type="number" value="0">
		</div>

		<div class="item-form">
			<label>今日の鴨</label>
			<input type="number" value="1"> 人
		</div>
		

    </form>

</body>

</html>